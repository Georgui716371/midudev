<p align="center" width="300">
   <img align="center" width="200" src="https://user-images.githubusercontent.com/1561955/106762302-fda9de00-6635-11eb-99be-3ef744e60c0e.png" />
   <h3 align="center">¡Hey 👋! Soy midudev 👨🏻‍💻</h3>
</p>

<p align="center">Soy <strong>Desarrollador JavaScript Full Stack</strong> con 10 años experiencia.<br />¡Revisa mi contenido 👇!</p>
<p align="center">
   <a href="https://twitch.tv/midudev" target="blank" style='margin-right:4px'>
    <img align="center" src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/twitch.svg" alt="midudev" height="28px" width="28px" />
  </a>
   <a href="https://youtube.com/midudev" target="blank" style='margin-right:4px'>
    <img align="center" src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/youtube.svg" alt="midudev" height="28px" width="28px" />
  </a>
  <a href="https://instagram.com/midu.dev" target="blank">
    <img align="center" src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/instagram.svg" alt="midu.dev" height="28px" width="28px" />
  </a>
  <a href="https://twitter.com/midudev" target="blank">
    <img align="center" src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/twitter.svg" alt="midudev" height="28px" width="28px" />
  </a>
</p>

### 📹 Últimos vídeos en mi [canal de Youtube](https://youtube.com/midudev?sub_confirmation=1)

<a href='https://youtu.be/2T5oJIhKWJI' target='_blank'>
  <img width='30%' src='https://img.youtube.com/vi/2T5oJIhKWJI/mqdefault.jpg' alt='Mejora en programación sin pagar dinero' />
</a>
<a href='https://youtu.be/vqCe3oG5sfY' target='_blank'>
  <img width='30%' src='https://img.youtube.com/vi/vqCe3oG5sfY/mqdefault.jpg' alt='Animaciones brutales para tu página web de copiar y pegar' />
</a>
<a href='https://youtu.be/nPKnehcfczA' target='_blank'>
  <img width='30%' src='https://img.youtube.com/vi/nPKnehcfczA/mqdefault.jpg' alt='La mejor terminal para programadores con Inteligencia Artificial y gratuita' />
</a>

### 📹 Últimos vídeos en mi [canal secundario de Youtube](https://youtube.com/midulive?sub_confirmation=1)

<a href='https://youtu.be/kNrCbOaRKSw' target='_blank'>
  <img width='30%' src='https://img.youtube.com/vi/kNrCbOaRKSw/mqdefault.jpg' alt='¡Usamos IA para desarrollar un proyecto con Node.js!' />
</a>
<a href='https://youtu.be/qkzcjwnueLA' target='_blank'>
  <img width='30%' src='https://img.youtube.com/vi/qkzcjwnueLA/mqdefault.jpg' alt='Curso de React desde cero: Crea un videojuego y una aplicación para aprender useState y useEffect' />
</a>
<a href='https://youtu.be/7iobxzd_2wY' target='_blank'>
  <img width='30%' src='https://img.youtube.com/vi/7iobxzd_2wY/mqdefault.jpg' alt='Curso de React [2023]: De cero hasta crear tus primeros componentes con estado' />
</a>

### 📸 Mis últimas fotos en [mi Instagram](https://instagram.com/midu.dev)

<a href='https://instagram.com/p/CqsqAHrM5VP' target='_blank'>
  <img width='20%' src='https://scontent-lhr8-2.cdninstagram.com/v/t51.2885-15/339791862_236963782227578_1117492710624537898_n.jpg?stp=dst-jpg_e15&_nc_ht=scontent-lhr8-2.cdninstagram.com&_nc_cat=102&_nc_ohc=HzOKMssFMUMAX_mbmNN&edm=APU89FABAAAA&ccb=7-5&oh=00_AfCvj25EggFaAdiYWuJLRMhd4GcWNZqXnHT3-Qc_HLhT_A&oe=64348B80&_nc_sid=86f79a' alt='Instagram photo' />
</a>
<a href='https://instagram.com/p/CqqHQgUPVx7' target='_blank'>
  <img width='20%' src='https://scontent-lhr8-1.cdninstagram.com/v/t51.2885-15/339795161_132583422987159_6413654429722198765_n.jpg?stp=dst-jpg_e15&_nc_ht=scontent-lhr8-1.cdninstagram.com&_nc_cat=103&_nc_ohc=GI2_azFcloQAX8GRgS0&edm=APU89FABAAAA&ccb=7-5&oh=00_AfCep0RROfK2pfLtW-gzEcvGuFRjWs1068QNuPDUOFnOlA&oe=6434B7CD&_nc_sid=86f79a' alt='Instagram photo' />
</a>
<a href='https://instagram.com/p/Cqnc-h0DNkh' target='_blank'>
  <img width='20%' src='https://scontent-lhr8-1.cdninstagram.com/v/t51.2885-15/339686413_892590691839007_5701461204965002874_n.jpg?stp=dst-jpg_e15&_nc_ht=scontent-lhr8-1.cdninstagram.com&_nc_cat=107&_nc_ohc=NI36ynIA_rYAX-lASol&edm=APU89FABAAAA&ccb=7-5&oh=00_AfAYCzHHQcGfLXcGFUc4bwKm-yI0hZaGEGWVPYBfelDzOg&oe=643452C4&_nc_sid=86f79a' alt='Instagram photo' />
</a>
<a href='https://instagram.com/p/CqljzZ1jqCm' target='_blank'>
  <img width='20%' src='https://scontent-lhr8-1.cdninstagram.com/v/t51.2885-15/339104206_195749186540889_3395561200941594724_n.jpg?stp=dst-jpg_e35_p1080x1080&_nc_ht=scontent-lhr8-1.cdninstagram.com&_nc_cat=109&_nc_ohc=IqZWGPziha8AX_uto1K&edm=APU89FABAAAA&ccb=7-5&oh=00_AfB1Y_kFVYQnfStwLNETQ3K3xQOPlywPk0uaDq8vNvNnmQ&oe=64380362&_nc_sid=86f79a' alt='Instagram photo' />
</a>

### 📝 Últimos artículos en mi [blog de Desarrollo Full Stack: midu.dev](https://midu.dev)
- [Top 5 preguntas de JavaScript en Stack Overflow](https://midu.dev/top-5-preguntas-javascript-stack-overflow/)
- [toReversed, toSpliced, toSorted y with. Nuevos métodos de Array en JavaScript explicados.](https://midu.dev/to-reversed-to-spliced-to-sorted-with/)
- [Cómo leer, copiar y pegar del portapapeles en JavaScript](https://midu.dev/leer-copiar-pegar-portapapeles-javascript/)
- [Desactivar reglas de eslint](https://midu.dev/desactivar-reglas-eslint/)
- [Curso de Rust para desarrolladores JavaScript](https://midu.dev/rust-para-desarrolladores-javascript/)